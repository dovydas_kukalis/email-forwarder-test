# https://github.com/forwardemail/free-email-forwarding/blob/master/SELF-HOSTING.md

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.2/install.sh | bash

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

nvm install 12

sudo add-apt-repository -y ppa:chris-lea/redis-server
sudo apt-get update
sudo apt-get -y install redis-server

sudo apt-get -y install spamassassin spamc python3 python3-pip

sudo apt-get -y install ufw

# allow port 22
sudo ufw allow ssh
# allow port 25
sudo ufw allow smtp
# allow port 465
sudo ufw allow smtps
# allow port 587
sudo ufw allow submission
# turn on rules
sudo ufw enable

npm install -g pm2

sudo apt-get install openssl
openssl genrsa -out private.key 1024
openssl rsa -in private.key -pubout -out public.key

pip3 install pyspf
pip3 install dnspython
pip3 install dkimpy

nano ~/.bashrc
export NVM_DIR="/root/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
